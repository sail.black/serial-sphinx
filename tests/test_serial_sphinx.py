from serial_sphinx.amazing_module import print_amazing_module, add, int_add
from serial_sphinx.sub_app.nice_module import print_nice_module
from tests.prepare import data_set

def test_woodpecker(data_set):
    data_set = data_set
    assert data_set == 5, "Something with pytest.fixture is not working"
    print_amazing_module()
    print_nice_module()
    assert 1 == 1, "Something does not work"

def test_add(): 
    res_1 = add(2, 3)
    assert res_1 == 5
    res_2 = int_add()
    assert res_2 == 3