# serial-sphinx

[![License: AGPL v3](https://img.shields.io/badge/License-AGPL_v3-blue.svg)](https://www.gnu.org/licenses/agpl-3.0)
![Python Versions](https://img.shields.io/badge/python-3.7%20%7C%203.9%20%7C%203.10%20%7C%20-blue) 
![Style Black](https://warehouse-camo.ingress.cmh1.psfhosted.org/fbfdc7754183ecf079bc71ddeabaf88f6cbc5c00/68747470733a2f2f696d672e736869656c64732e696f2f62616467652f636f64652532307374796c652d626c61636b2d3030303030302e737667) 
[![status-badge](https://ci.codeberg.org/api/badges/sail.black/serial-sphinx/status.svg)](https://ci.codeberg.org/sail.black/serial-sphinx)
[![Documentation Status](https://readthedocs.org/projects/serial-sphinx/badge/?version=latest)](https://serial-sphinx.readthedocs.io/en/latest/?badge=latest)
# Why?

Rapid prototyping requires boilerplate code

# What?

`serial-sphinx` distills field knowledge about using sphinx for uploading your documentation to [readthedocs.org](www.readthedocs.org). 
Follow the [blog](https://medium.com/sail-black/get-your-docs-published-on-readthedocs-using-python-sphinx-in-less-than-7-minutes-using-5-87b10e651000) for more useful info. 

# Your To-Do-List 

* rename the folder `serial_sphinx/` and replace the name in the `.woodpecker.yml` with the package name of your package

* prepare `setup.py` for your case 

* set-up webhook for `readthedocs.org`

* set-up Woodpecker in the web interface

* annotate your library with `MonkeyType`

# Essential automatation 

Make sure to 
```
pip install black autopep8 
```
And run 

```
black serial_sphinx
autopep8 --in-place --recursive serial_sphinx/
```

Also, you may use the local CI script `local-ci.sh` to save computation time of free CI resources

```
bash local-ci.sh
```

# Command snippets

On debian you may have to install some tools 

```
apt-get install python3-sphinx
```


```
rm -r docs 
mkdir docs
cd docs
sphinx-quickstart
```
Do build seperate directories.

Copy the config file from the repository. Then, in the `docs` folder
```
sphinx-apidoc -o source/ ../<package_name>
make html
```
Make sure you follow the appropriate file structure outlined in the repository. 

Push it to you repo with the configured webhook, and you are done. 

# Proper testing
Please make sure to setup some CI/CD pipeline. If you are avoiding commercial setups, then this is a problem. 

We provided you with a reasonable CI through the `woodpecker.yml`. However, we do not recommend to deploy via `codeberg.org`. 

You have to reduce the risk of data leaks! You have to setup your own system. 


For code quality, make sure you have great coverage by using `pytest-cov`. Check out the provided `setup.cfg` file. 

# Woodpecker CI 

Open-source CI. But not like Github-actions. Check the `.woodpecker.yml` for a tested standard workflow on `CodebergCI`. 

# Code-Coverage

The passing goal for the code-coverage should be edited in the `setup.cfg` under the key `addopts`

# Standardizing code 

There are multiple steps necessary for this. Firstly, you have to prepare the code preferrably with `black` locally. After installation via `pip`

```bash
pip install black
```
Run
```bash
black. 
```
The CI-pipeline includes a way to standardize the code such that no serious bugs are present. 

The standardizing approach includes 

* Linting (`pylint`)
* Checking for errors (`pyflake8`)
* typing (`mypy`)

Essential files are 

* `setup.cfg`
* `.pylintrc`

# Typing in Python 

Even though typing is not a feature of the Python language, it can be enforced using `mypy`. We recommend, that any new code you write from now on uses typing. 

Typing will improve the quality of your code even further. 

Check the [mypy documentation](http://mypy-lang.org/examples.html) for details. 

## Help to annotate your library 

Typing is absolutely mandatory for our codebase. 

Nice reference on how to annotate from [PyCon2018](https://speakerdeck.com/pycon2018) from [Carl Meyer
](https://speakerdeck.com/pycon2018/carl-meyer-type-checked-python-in-the-real-world)
```
monkeytype apply some.module
```

To initialize an existing codebase use `MonkeyPy`
```
pip install MonkeyType
```
You have to generate the types calling a script that calls the methods you want to type

```bash
monkeytype run myscript.py #calls a lot of methods
```
Then, generate the stub file 

```bash 
monkeytype stub some.module
```
Now, change the file 

```bash
monkeytype apply some.module
```
to modify `some/module`

Even better way is to use MonkeType with pytest. 
There is an extension. Check their [website](https://pypi.org/project/pytest-monkeytype/)

```bash
pip install pytest-monkeytype
```


## Generate custom stubs with `stubgen`

You need annoted files first. 

```
git commit -m"stubs using stubgen"
```

After double checking and rediting they are ready for use in your package.

