serial\_sphinx.sub\_app package
===============================

Submodules
----------

serial\_sphinx.sub\_app.nice\_module module
-------------------------------------------

.. automodule:: serial_sphinx.sub_app.nice_module
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: serial_sphinx.sub_app
   :members:
   :undoc-members:
   :show-inheritance:
