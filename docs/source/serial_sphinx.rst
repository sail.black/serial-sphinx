serial\_sphinx package
======================

Subpackages
-----------

.. toctree::
   :maxdepth: 4

   serial_sphinx.sub_app

Submodules
----------

serial\_sphinx.amazing\_module module
-------------------------------------

.. automodule:: serial_sphinx.amazing_module
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: serial_sphinx
   :members:
   :undoc-members:
   :show-inheritance:
