blkp serial_sphinx/
autopep8 --in-place --recursive serial_sphinx/
python -m flake8 . --count --select=E9,F63,F7,F82 --show-source --statistics
mypy --strict kinkywords/
python -m pylint -f parseable serial_sphinx/*.py 
pytest tests/