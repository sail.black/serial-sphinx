"""
The module docstring aims to score high on pylint
"""

def print_amazing_module()-> None:
    """
    The print_amazing_module function prints the string 'amazing module' to the console.
    
    :return: The string "amazing module".
    
    :doc-author: Trelent
    """
    
    print("amazing module")

def add(int_1:int, int_2:int)-> int: 
    """
    The add function adds two numbers and returns the result.

    :param a: Used to Pass the first number in the addition.
    :param b: Used to Add a value to the a parameter.
    :return: The sum of the two numbers that were passed to it.

    :doc-author: Trelent
    """

    return int_1+int_2

def int_add(int_1:int =1, int_2:int =2)->int:
    """
    The int_add function adds two integers together.
    Test MonkeyType
    Args:
        x (int): The first integer to add.

        y (int): The second integer to add.

    Returns:
        int: The sum of the two integers added together.

    :param 1: Used to Represent the first number to be added.
    :param 2: Used to Define the number of times that the add function is going to be called.
    :return: The result of add(a, b).

    :doc-author: Trelent
    """

    return add(int_1, int_2)