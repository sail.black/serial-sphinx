"""
This module is for testing purposes only. The docstring ensures
good pylint scores.
"""

def print_nice_module()-> None:
    """
    The print_nice_module function prints the string 'nice module in subapp.' to the console.
    
    :return: The string "nice module in subapp.
    
    :doc-author: Trelent
    """
    
    print("nice module in subapp.")
